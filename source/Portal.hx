package;

	import flixel.FlxG;
	import flixel.group.FlxGroup;
	import flixel.FlxObject;
	import flixel.FlxSprite;
	
	class Portal extends FlxSprite
	{
		
		public var num:Int = 0;
		private var isShrinking:Bool = false;
		private var entrance:Bool = false;
		public var used:Bool = false;
		
		public function new(X:Float, Y:Float, N:Int, ENT:Bool, ?M:Bool = false)
		{
			
			super(X*128, Y*128);
			
			if(M)
				velocity.x = 400;
			
			num = N;
			
			loadGraphic("assets/images/portalsprite.png", true, 300, 300);
			
			animation.add("entrance",[0,2],2,true);
			animation.add("exit",[1,3],2,true);

			height = 128;
			offset.y = 86;
			
			width = 128;
			offset.x = 86;
			
			entrance = ENT;
			
			if(ENT)
			{
				animation.play("entrance");				
			}
			else
			{
				animation.play("exit");
				scale.x = scale.y = 0;
			}
			
			
		}
		
		
		override public function update(elapsed:Float):Void
		{
			if(x < 0)
			{
				x = 0;
				velocity.x = 400;
			}
			if(x > 1920-128)
			{
				x = 1920-128;
				velocity.x = -400;
			}
			
			if(isShrinking)
			{
				velocity.x = 0;
				
				if(entrance)
				{
					scale.x -= 0.05;
					scale.y -= 0.05;
				
					if(scale.x <= 0)
					{
						isShrinking = false;
						kill();
					}
				}
				else
				{
					scale.x += 0.05;
					scale.y += 0.05;
					
					if(scale.x >= 1)
						isShrinking = false;
				}
			}
			
			super.update(elapsed);			
			
		}
		
		
		public function shrink():Void
		{
			used = true;
			isShrinking = true;
		}
		
		
		override public function draw():Void
		{
			//playerTrail.draw();
			super.draw();
		}
		
		
	}
