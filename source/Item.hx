package ;

	import flixel.FlxSprite;
	import flixel.FlxObject;
	
	class Item extends FlxSprite
	{
		
		public function new(X:Float, Y:Float, T:Int)
		{
			super(X*16, Y*16);
			
			type = T;
			
			//loadGraphic("assets/images/itemsprite.png", true, 16, 16);
			
			
		}
		
		
		
		override public function update(elapsed:Float):Void
		{
			
			super.update(elapsed);
		}
		
		override public function draw():Void
		{
			super.draw();
		}
		
	}
