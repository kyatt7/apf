package; 

	import flixel.FlxSprite;
	import flixel.FlxObject;
	import flixel.text.FlxText;
	import flixel.FlxState;
	import flixel.FlxG;
	import flixel.group.FlxGroup;
	import flixel.util.FlxSort;
	import flixel.math.FlxMath;
	import flixel.util.FlxTimer;
	import flixel.math.FlxRandom;
	import flixel.tile.FlxTilemap;
	
	import flixel.input.gamepad.FlxGamepad;
	import flixel.input.gamepad.FlxGamepadInputID;
	import openfl.Assets;
	
	class PlayState extends FlxState
	{
		private var gameOver:Bool = false;
		
		private var aKeys:Array<flixel.input.keyboard.FlxKey>;
		
		private var airJump:Bool = false;
		
		private var _gamePad:FlxGamepad;
		private var background:FlxTilemap = new FlxTilemap();
		private var railing:FlxTilemap = new FlxTilemap();
		private var foreground:FlxTilemap = new FlxTilemap();
		private var portalMap:FlxTilemap = new FlxTilemap();
		private var floor:FlxSprite;
		private var brother:FlxSprite;
		private var arrow:FlxSprite;
		private var cameraFollow:FlxObject = new FlxObject(0,0);
		private var clock:FlxSprite;
		private var speechBubble:FlxSprite;
		private var player:Player;
		private var isShifting:Bool = false;
		private var isCrouching:Bool = false;
		private var isTeleporting:Bool = false;
		private var pressedThisFrame:Bool = false;
		
		private var numbers:Array<FlxSprite> = new Array<FlxSprite>();
		
		private var targetPortal:Portal;
		
		private var minutes:Int = 0;
		private var timeElapsed:Float = 0;
		
		private var entrances:FlxTypedGroup<Portal> = new FlxTypedGroup<Portal>();
		private var exits:FlxTypedGroup<Portal> = new FlxTypedGroup<Portal>();
		
		private var angleChange:Float = 2;
		
		private var i:Int = 0;
		private var j:Int = 0;
		private var tx:Int = 0;
		private var ty:Int = 0;
		
		public function new() 
		{
			super();
		}
		
		override public function create():Void
		{
			FlxG.camera.antialiasing = Reg.antiAlias;
			
			aKeys = [SPACE, Z,X,C,V,B,N,M];
			
			//entrances.add(new Portal(300,500,0,true));
			//exits.add(new Portal(1300,250,0,false));
			
			railing.loadMapFromCSV(Assets.getText("assets/maps/mapCSV_Group1_railing.csv"), "assets/images/railing.png", 128, 128, null, 0, 0, 1);
			railing.y += 8;

			foreground.loadMapFromCSV(Assets.getText("assets/maps/mapCSV_Group1_Map.csv"), "assets/images/tilemap.png", 128, 128, null, 0, 0, 1);
			foreground.setTileProperties(1, FlxObject.UP, null, null, 1);
			foreground.setTileProperties(2, FlxObject.UP, null, null, 1);
			
			for (tx in 0...foreground.widthInTiles)
				for (ty in 0...foreground.heightInTiles)
				{
					if(foreground.getTile(tx, ty) == 1 && FlxG.random.int(0,2) == 1)
						foreground.setTile(tx, ty,2);
				}		
				
			background.loadMapFromCSV(Assets.getText("assets/maps/mapCSV_Group1_Background.csv"), "assets/images/background.jpg", 1920, 700, null, 0, 0, 1);
			//background.y += 1000;
			background.scrollFactor.y = 0.4;
			
			brother = new FlxSprite(0,0,"assets/images/brother.png");
		
			speechBubble = new FlxSprite(0,0);
			speechBubble.loadGraphic("assets/images/speechbubble.png",true,120,135);
			speechBubble.animation.add("off",[0],1,false);
			speechBubble.animation.add("on",[0,1,2,3],2,false);
			speechBubble.animation.add("bro",[4],2,false);
			speechBubble.animation.play("off");
			
			portalMap.loadMapFromCSV(Assets.getText("assets/maps/mapCSV_Group1_Portals.csv"), "assets/images/map.png", 16, 16);
		
			var entNum:Int = 0;
			var extNum:Int = 0;
			for (ty in 0...portalMap.heightInTiles)
				for (tx in 0...portalMap.widthInTiles)
				{
					var hit:Int = portalMap.heightInTiles-1;
					if(portalMap.getTile(tx,hit-ty) == 1)
					{
						entrances.add(new Portal(tx,hit-ty,entNum,true));
						entNum++;
					}
					else if(portalMap.getTile(tx,hit-ty) == 3)
					{
						entrances.add(new Portal(tx,hit-ty,entNum,true,true));
						entNum++;
					}
					else if(portalMap.getTile(tx,hit-ty) == 2)
					{
						exits.add(new Portal(tx,hit-ty,extNum,false));
						extNum++;
					}
					else if(portalMap.getTile(tx,hit-ty) == 4)
					{
						brother.x = tx*128;
						brother.y = ((hit-ty)*128)-28;
						speechBubble.x = brother.x + 40;
						speechBubble.y = brother.y - 120;
					}
					
				}
			
			exits.add(new Portal(8,117,24,false));
			
			
			
			FlxG.mouse.visible = false;
			
			floor = new FlxSprite(0,920,"assets/images/floor.png");
			floor.solid = true;
			floor.immovable = true;

			arrow = new FlxSprite(0,0,"assets/images/arrowsprite.png");

			clock = new FlxSprite(0,0,"assets/images/clock.png");
			clock.scrollFactor.y = 0;
			
			numbers[0] = new FlxSprite(128,25);
			numbers[1] = new FlxSprite(214,25);
			numbers[2] = new FlxSprite(337,25);
			numbers[3] = new FlxSprite(423,25);
			numbers[4] = new FlxSprite(538,25);
			
			for(i in 0...5)
			{
				numbers[i].scrollFactor.y = 0;
				numbers[i].loadGraphic("assets/images/numbers.png",true,100,120);
				for(j in 0...10)
					numbers[i].animation.add(""+j,[j],1,false);
				numbers[i].animation.play("0");
			}
			
			
			player = new Player(1000,12400+(128*20));
			cameraFollow.x = player.x;
			cameraFollow.y = player.y+200;
			
			if(Reg.repeat)
			{
				player.velocity.y = -200;
				exits.members[24].shrink();
			}
			
			add(background);
			add(foreground);
			add(railing);
//			add(floor);
			add(entrances);
			add(exits);
			add(arrow);
			add(player);
			add(brother);
			add(speechBubble);
			add(clock);
			for(i in 0...5)
				add(numbers[i]);
			FlxG.camera.setScrollBoundsRect(0, 0-1000, foreground.width, foreground.height+1000);
			FlxG.worldBounds.set(0, 0-1000, foreground.width, foreground.height+1000);
			
			FlxG.camera.follow(cameraFollow, 1);

		}
		
		override public function update(elapsed:Float):Void
		{
			super.update(elapsed);
			
			player.visible = !isShifting;
			
			if(!isShifting)
			{
				cameraFollow.y = player.y;//+(player.velocity.y/15);
			}
			else
			{
				if(gameOver)
				{
					cameraFollow.y += 36;
					if(cameraFollow.y > foreground.height)
					{
						Reg.repeat = true;
						FlxG.switchState(new PlayState());
					}
				}
				else
				{
					if(cameraFollow.y > exits.members[targetPortal.num].y - 150)
						cameraFollow.y -= 18;
					else if(cameraFollow.y < exits.members[targetPortal.num].y - 150)
						cameraFollow.y += 18;
					
					if(exits.members[targetPortal.num].y-140 > cameraFollow.y && exits.members[targetPortal.num].y-160 < cameraFollow.y)
					{
						exits.members[targetPortal.num].shrink();

						isShifting = false;
						isTeleporting = false;
						player.angle = 0;
						player.x = player.last.x = exits.members[targetPortal.num].x;
						player.y = player.last.y = exits.members[targetPortal.num].y - 150;
			
						player.acceleration.y = 1500;
					
						player.velocity.x = 0;
						player.velocity.y = -200;
			
						angleChange++;
						player.nextOne();
						//player.form = (player.form + 1) % 5;
						player.animation.play("jump"+player.form);
					}
					
				}
			}
			
			if(player.x < 0)
			{
				player.x = 0;
				player.velocity.x = 0;
			}
			if(player.x >1820)
			{
				player.x = 1820;
				player.velocity.x = 0;
			}
			
			arrow.visible = isCrouching;
			arrow.x = player.x-150;
			arrow.y = player.y-100;
			
			_gamePad = FlxG.gamepads.lastActive;
			
			if(!gameOver)
			{
				timeElapsed += FlxG.elapsed;

				minutes = Std.int(timeElapsed/60);
			
				numbers[0].animation.play(""+Std.int(minutes/10)%10);
				numbers[1].animation.play(""+Std.int(minutes)%10);
				numbers[2].animation.play(""+Std.int((timeElapsed-(minutes*60))/10)%10);
				numbers[3].animation.play(""+Std.int(timeElapsed)%10);
				numbers[4].animation.play(""+Std.int(timeElapsed*10)%10);				
			}
			
			FlxG.collide(player, floor);
			FlxG.collide(player, foreground);
			FlxG.overlap(player, entrances, teleport);
			
			if(!gameOver && player.isTouching(FlxObject.FLOOR) && player.y < (brother.y+100))
			{
				exits.members[24].visible = false;
				gameOver = true;
				if(player.form == 0 || player.form == 24)
					speechBubble.animation.play("bro");
				else
					speechBubble.animation.play("on");
			}
			
			if(isTeleporting && !isShifting)
			{
				player.angle += 12;
				player.x = (player.x + targetPortal.x+20)/2;
				player.y = (player.y + targetPortal.y-30)/2;
					
				if(player.angle > 360)
				{
					isShifting = true;
					targetPortal.shrink();
					
					
				}
				
			}
			
			if(isCrouching)
			{
				arrow.angle += angleChange;
				
				if(arrow.angle > 0)
					player.flipX = false;
				else if(arrow.angle < 0)
					player.flipX = true;
				
				if(arrow.angle > 45)
				{
					arrow.angle = 45;
					angleChange = -2;
				}
				if(arrow.angle < -45)
				{
					arrow.angle = -45;
					angleChange = 2;
				}
				
			}
			
			if(player.isTouching(FlxObject.FLOOR))
				player.velocity.x = 0;
			
			pressedThisFrame = false;
			
			if (_gamePad != null)
			 	gamepadControls();

			controls();
			
			if(!pressedThisFrame)
			{
				if(player.isTouching(FlxObject.FLOOR))
				{
					player.animation.play("idle"+player.form);
					airJump = true;
				}
				
				if(isCrouching)
				{
					
					FlxG.sound.play("assets/sounds/jump1.ogg", 1, false, false);
					
					player.velocity.x = Math.cos((arrow.angle+90)/(180/3.1419))*-1000;
					player.velocity.y = Math.sin((arrow.angle+90)/(180/3.1419))*-1500;
					
					isCrouching = false;
					player.animation.play("jump"+player.form);
					//player.velocity.y = -1500;	
				}
				
			}
			//
// 			if(FlxG.keys.anyJustPressed([ONE]))
// 			{
// 				angleChange++;
// 				player.nextOne();
// //				player.form = (player.form + 1) % 5;
// 			}
			if(FlxG.keys.anyJustPressed([R]))
				FlxG.switchState(new PlayState());
			if(FlxG.keys.anyJustPressed([ESCAPE]))
			{
				FlxG.sound.music.stop();
				FlxG.switchState(new TitleState());
			}
			// if(FlxG.keys.anyJustPressed([DOWN]))
			// 	background.y += 1000;
			// if(FlxG.keys.anyJustPressed([UP]))
			// 	background.y -= 1000;
			if(FlxG.keys.anyJustPressed([F]))
				FlxG.fullscreen = !FlxG.fullscreen;

			if(FlxG.keys.anyJustPressed([A]))
			{
				FlxG.camera.antialiasing = !FlxG.camera.antialiasing;
				Reg.antiAlias = !Reg.antiAlias;
			}
			
			
		}
		
		private function teleport(P:Player, E:Portal):Void
		{
			if(isTeleporting || E.used)
				return;
			
			FlxG.sound.play("assets/sounds/portal.ogg", 1, false, false);
			
			player.velocity.x = 0;
			player.velocity.y = 0;
			player.acceleration.y = 0;
			isTeleporting = true;
			targetPortal = E;
			
		}
		
		private function gamepadControls():Void
		{
			if(_gamePad.pressed.A || _gamePad.pressed.B || _gamePad.pressed.X || _gamePad.pressed.Y)
				pressA();
		}
		
		private function controls():Void
		{
			if(FlxG.keys.anyPressed(aKeys))
				pressA();
		}

		private function pressA():Void
		{
			if(isTeleporting)
				return;
			
			pressedThisFrame = true;

			if(!player.isTouching(FlxObject.FLOOR) && airJump)
			{
				airJump = false;
				FlxG.sound.play("assets/sounds/jump2.ogg", 1, false, false);
				player.velocity.y = -900;
				player.velocity.x = player.velocity.x * 0.1;
				player.animation.play("airjump"+player.form);
				
			}

			if(!isCrouching && player.isTouching(FlxObject.FLOOR))
			{
				arrow.angle = 0;
				angleChange = (player.flipX)?-2:2;
				player.animation.play("crouch"+player.form);
				isCrouching = true;
				//show meter
			}	
		}


	
	}