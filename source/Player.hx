package;

	import flixel.FlxG;
	import flixel.group.FlxGroup;
	import flixel.FlxObject;
	import flixel.FlxSprite;
	
	class Player extends FlxSprite
	{
		private var i:Int = 0;
		public var form:Int = 0;
		
		private var page:Int = 0;
		
		public function new(X:Float, Y:Float)
		{
			
			super(X, Y);
			
			loadGraphic("assets/images/playersprite1.png", true, 300, 300);
			
			for(i in 0...5)
			{
				animation.add("idle"+i,[(i*3)],4,false);
				animation.add("crouch"+i,[(i*3)+1],4,false);
				animation.add("jump"+i,[(i*3)+2],4,false);
				animation.add("airjump"+i,[(i*3)+1,(i*3)+2],8,false);
				
			}
			
			maxVelocity.y = 1500;
			
			height = 200;
			offset.y = 60;
			
			width = 100;
			offset.x = 100;
			
			animation.play("idle0");
			
			acceleration.y = 1500;
			
		}
		
		public function nextOne():Void
		{
			if(form >= 24)
				return;
			
			form++;
			if(form % 5 == 0)
			{
				page++;
				loadGraphic("assets/images/playersprite"+(page+1)+".png", true, 300, 300);
			
				height = 200;
				offset.y = 60;
			
				width = 100;
				offset.x = 100;
				
				for(i in 0...5)
				{
					animation.add("idle"+(i+(page*5)),[(i*3)],4,false);
					animation.add("crouch"+(i+(page*5)),[(i*3)+1],4,false);
					animation.add("jump"+(i+(page*5)),[(i*3)+2],4,false);
					animation.add("airjump"+(i+(page*5)),[(i*3)+1,(i*3)+2],8,false);
				
				}
				
				animation.play("idle"+(page*5));
			}
		}
		
		override public function update(elapsed:Float):Void
		{
			if(velocity.x > 0)
				flipX = false;
			if(velocity.x < 0)
				flipX = true;
			
			super.update(elapsed);			
			
		}
		
		
		
		
		override public function draw():Void
		{
			//playerTrail.draw();
			super.draw();
		}
		
		
	}
