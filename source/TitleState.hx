package; 

	import flixel.FlxSprite;
	import flixel.FlxObject;
	import flixel.text.FlxText;
	import flixel.FlxState;
	import flixel.FlxG;
	import flixel.group.FlxGroup;
//	import flixel.group.FlxTypedGroup;
	import flixel.util.FlxSort;
	import flixel.math.FlxMath;
	import flixel.util.FlxTimer;
	import flixel.math.FlxRandom;
	import flixel.tile.FlxTilemap;
	
	import flash.system.System;
	
	import flixel.input.gamepad.FlxGamepad;
	import flixel.input.gamepad.FlxGamepadInputID;
	
	class TitleState extends FlxState
	{
		
		private var title1:FlxSprite;
		private var title2:FlxSprite;
		private var title3:FlxSprite;
		private var title4:FlxSprite;
		private var title5:FlxSprite;
		
		private var titleTimer:Int = 0;
		
		private var screen:Int = 1;
		
		private var _gamePad:FlxGamepad;
		
		public function new() 
		{
			super();
		}
		
		override public function create():Void
		{
			
			Reg.repeat = false;
			
			FlxG.camera.antialiasing = Reg.antiAlias;
			
			FlxG.mouse.visible = false;
		
			title1 = new FlxSprite(0,0,"assets/images/ggj.png");
			title2 = new FlxSprite(0,0,"assets/images/bgs.png");
		
			title3 = new FlxSprite(0,0,"assets/images/title1.jpg");
			title4 = new FlxSprite(1920,0,"assets/images/title2.jpg");
			title5 = new FlxSprite(-1920,0,"assets/images/title3.jpg");
			
			add(title3);
			add(title4);
			add(title5);
			add(title2);
			add(title1);
			
		}
		
		override public function update(elapsed:Float):Void
		{
			super.update(elapsed);
			
			if(screen == 1)
			{
				titleTimer++;
				if(titleTimer >= 120)
				{
					title1.visible = false;
					screen = 2;
					titleTimer = 0;
				}
			}
			else if(screen == 2)
			{
				titleTimer++;
				if(titleTimer >= 120)
				{
					title2.visible = false;
					screen = 3;
					FlxG.sound.playMusic("assets/music/music.ogg", 0.8, true);
			
					titleTimer = 0;
				}
			}
			
			_gamePad = FlxG.gamepads.lastActive;
			if (_gamePad != null)
				gamepadControls();
			
			controls();

			if(FlxG.keys.anyJustPressed([ESCAPE]))
				System.exit(0);
			
			if(FlxG.keys.anyJustPressed([F]))
				FlxG.fullscreen = !FlxG.fullscreen;
			if(FlxG.keys.anyJustPressed([A]))
			{
				FlxG.camera.antialiasing = !FlxG.camera.antialiasing;
				Reg.antiAlias = !Reg.antiAlias;
			}
			
			if(title4.x < 0)
			{
				title4.x = 0;
				title4.velocity.x = 0;
			}
			if(title5.x > 0)
			{
				title5.x = 0;
				title5.velocity.x = 0;
			}
			
		}
		
		private function nextPage():Void
		{
			if(screen == 1 || screen == 2)
			{
				title1.visible = false;
				title2.visible = false;
				screen = 3;
				FlxG.sound.playMusic("assets/music/music.ogg", 0.8, true);
				
			}
			else if(screen == 3)
			{
				screen++;
				title4.velocity.x = -3000;
			}
			else if(screen == 4)
			{
				screen++;
				title5.velocity.x = 3000;
			}
			else
			{
				FlxG.switchState(new PlayState());
			}
		}
		
		private function gamepadControls():Void
		{
			if(_gamePad.justPressed.A || _gamePad.justPressed.B || _gamePad.justPressed.X || _gamePad.justPressed.Y || _gamePad.justPressed.START)
				nextPage();
			
		}
		
		private function controls():Void
		{
			if(FlxG.keys.anyJustPressed([ ENTER, SPACE,Z,X,C,V,B,N,M]))
				nextPage();

		}
	}
	